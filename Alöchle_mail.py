#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 26 08:20:33 2020

@author: timonbaltisberger

"""

import numpy as np
import random
from mailing import send_hand as s_h

# parameters of playing cards
values_jass_french = ["6","7","8","9","10","Buur","Dame","König","Ass"]
values_jass_german = ["6","7","8","9","10","Onder","Ober","König","Ass"]
colors_french = ["Härz   ", "Chrüz  ", "Schufle", "Egge   "]
colors_german = ["Eichle ","Röseli ", "Schäue ", "Schöute"]

# card settings
type_of_cards = input("Met französische Charte spele? Denn tipp 'y' & met Enter bestätige. Sösch gehts Dütschi. ")
if type_of_cards == 'y': 
    colors = colors_french
    values = values_jass_french
else: 
    colors = colors_german
    values = values_jass_german

card_set = ["- {} {}".format(color, value) for value in values for color in colors]

# player settings
N_players = int(input("Wievöu speler sender? "))
N_total_cards = len(card_set)
N_cards_player = N_total_cards/N_players
if N_cards_player%1 != 0:
    print("Das esch schwerig. I neme die töifste Charte use besses ufgoht.\n")
    removed_cards =[]
    
    while N_cards_player % 1 != 0:
        removed_cards.append(card_set.pop(0))
        N_cards_player = len(card_set)/N_players
    
    print("I ha die Charte usegnoh:\n","\n".join(removed_cards))
        
while True:
    player_names_mails_raw = input("Geb d'Näme vode Speler ond s'jewiilige Mail met Koma trennt a, ohni Läärzeiche!\n")
    #player_names_mails_raw = "1,timon.baltisberger@gmail.com,2,timon.baltisberger@gmail.com,3,timon.baltisberger@gmail.com,4,timon.baltisberger@gmail.com,5,timon.baltisberger@gmail.com"
    player_names_mails = player_names_mails_raw.split(",")
    if len(player_names_mails) == 2*N_players:
        player_names = [player_names_mails[2*ii] for ii in range(N_players)]
        player_mails = [player_names_mails[2*ii+1] for ii in range(N_players)]
        break
    print("Das het ned klappet, probiers nomou")

# let's go
counter = 1
while True:
    cards_round = card_set
    random.shuffle(card_set)
    cards_round_np = np.reshape(np.array(cards_round), (int(N_cards_player), N_players))

    for ii in range(N_players):
        name = player_names[ii]
        hand = cards_round_np[:,ii]
        
        hand_py = np.ndarray.tolist(hand)
        hand_py.sort()
        subject = subject = "S'esch Ziit zom Arschlöchle: Rondi {}".format(counter)
        txt_mail = "Dini Charte "+ player_names[ii] +": \n\n" + "\n".join(hand_py)
        s_h(subject, txt_mail,player_mails[ii])
        
    inpt = input("Dröck 'Enter' för nöii Charte oder schriib 'stopp' zom ufhöre\n")
    if inpt == 'stopp' or inpt == 'stop':
        print("\n \nBes zom nöchste mou")
        break   
# =============================================================================
#     stp = input("stop?")
#     if stp == "stop":
#         break
# =============================================================================
    
    counter += 1
